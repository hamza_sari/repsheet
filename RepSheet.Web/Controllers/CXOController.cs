using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RepSheet.Web.Data;
using RepSheet.Web.Models;

namespace RepSheet.Web.Controllers
{
    public class CXOController : Controller
    {
        private readonly RepSheetWebDbContext _context;

        public CXOController(RepSheetWebDbContext context)
        {
            _context = context;
        }

        // GET: CXO
        public async Task<IActionResult> Index()
        {
            return View(await _context.CXO.ToListAsync());
        }

        // GET: CXO/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cXO = await _context.CXO
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cXO == null)
            {
                return NotFound();
            }

            return View(cXO);
        }

        // GET: CXO/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CXO/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Abbrevition,FullName")] CXO cXO)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cXO);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cXO);
        }

        // GET: CXO/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cXO = await _context.CXO.FindAsync(id);
            if (cXO == null)
            {
                return NotFound();
            }
            return View(cXO);
        }

        // POST: CXO/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Abbrevition,FullName")] CXO cXO)
        {
            if (id != cXO.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cXO);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CXOExists(cXO.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cXO);
        }

        // GET: CXO/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cXO = await _context.CXO
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cXO == null)
            {
                return NotFound();
            }

            return View(cXO);
        }

        // POST: CXO/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cXO = await _context.CXO.FindAsync(id);
            _context.CXO.Remove(cXO);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CXOExists(int id)
        {
            return _context.CXO.Any(e => e.ID == id);
        }
    }
}
