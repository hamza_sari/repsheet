namespace RepSheet.Web.Models
{
    public class CXO
    {
        public int ID { get; set; }
        public string Abbrevition { get; set; }
        public string FullName { get; set; }
    }
}