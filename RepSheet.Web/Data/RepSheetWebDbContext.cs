using Microsoft.EntityFrameworkCore;
using RepSheet.Web.Models;

namespace RepSheet.Web.Data
{
    public class RepSheetWebDbContext : DbContext
    {
        public RepSheetWebDbContext() : base()
        { }
 
        public RepSheetWebDbContext(DbContextOptions<RepSheetWebDbContext> options) : base(options)
        { }
 
        public DbSet<CXO> CXO { get; set; }
    }
}